const mongoose = require("mongoose");
const Question = mongoose.model("Question");

function shuffle(o) {
	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}

module.exports = {
	async getAll(req, res) {
		const registros = await Question.find({}).populate("_level");
		return res.json(registros);
	},
	async getByID(req, res) {
		const registro = await Question.findById(req.params.id).populate("_level");
		return res.json(registro);
	},
	async incluir(req, res) {
		try {
			let registro = await Question.create(req.body);
			registro = await registro.populate("_level").execPopulate();
			return res.json(registro);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg });
		}
	},
	async editar(req, res) {
		const registro = await Question.findByIdAndUpdate(req.params.id, req.body, { new: true }).populate("_level");
		return res.json(registro);
	},
	async excluir(req, res) {
		await Question.findByIdAndRemove(req.params.id);
		return res.send();
	},
	async getAllQuestionByLevel(req, res) {
		const registros = await Question.find({ _level: { $in: [req.params.level] } }).populate("_level");
		return res.json(registros);
	},
	async getByLevel(req, res) {
		const fiftyQuestion = req.body.fiftyQuestion;
		const skippedQuestions = req.body.skippedQuestions;

		const skipQuestions = [];

		if (skippedQuestions.length > 0) {
			skippedQuestions.forEach(obj => {
				skipQuestions.push(obj);
			});
		}

		if (fiftyQuestion) {
			skipQuestions.push(fiftyQuestion);
		}

		const registros = await Question.find({ _level: { $in: [req.params.level] } }).populate("_level");

		const ret = registros.filter(objQuestion => {
			if (!skipQuestions.includes(objQuestion._id.toString())) {
				return objQuestion;
			}
		});

		return res.json(shuffle(ret)[0]);
	}
};
