const mongoose = require("mongoose");

const UserSchema = mongoose.Schema(
	{
		name: { type: String, require: true },
		email: { type: String, require: true, unique: true }
	},
	{ timestamps: true }
);

mongoose.model("User", UserSchema);
