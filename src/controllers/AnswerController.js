const mongoose = require("mongoose");
const Answer = mongoose.model("Answer");

function shuffle(o) {
	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}

module.exports = {
	async getAll(req, res) {
		const registros = await Answer.find({});
		return res.json(registros);
	},
	async getByID(req, res) {
		const registros = await Answer.findById(req.params.id);
		return res.json(registros);
	},
	async incluir(req, res) {
		try {
			const registro = await Answer.create(req.body);
			return res.json(registro);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg });
		}
	},
	async editar(req, res) {
		const registro = await Answer.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.json(registro);
	},
	async excluir(req, res) {
		await Answer.findByIdAndRemove(req.params.id);
		return res.send();
	},
	async getAnswerByQuestion(req, res) {
		const colValid = await Answer.find({ _question: req.params.id, isValid: true });
		const colInvalid = await Answer.find({ _question: req.params.id, isValid: false }).limit(3);

		const registros = [...colValid, ...colInvalid];

		return res.json(shuffle(registros));
	}
};
