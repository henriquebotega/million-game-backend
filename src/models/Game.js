const mongoose = require("mongoose");

const GameSchema = mongoose.Schema(
	{
		_user: { ref: "User", type: mongoose.Schema.Types.ObjectId },
		_question: { ref: "Question", type: mongoose.Schema.Types.ObjectId },
		_level: { ref: "Level", type: mongoose.Schema.Types.ObjectId },
		skip: { type: Number, default: 3 },
		fifty: { type: Boolean, default: false },
		_fiftyQuestion: { ref: "Question", type: mongoose.Schema.Types.ObjectId },
		_skippedQuestions: [{ ref: "Question", type: mongoose.Schema.Types.ObjectId }],
		reward: { type: String }
	},
	{ timestamps: true }
);

mongoose.model("Game", GameSchema);
