const mongoose = require("mongoose");

const QuestionSchema = mongoose.Schema(
	{
		description: { type: String, require: true },
		_level: { ref: "Level", type: mongoose.Schema.Types.ObjectId }
	},
	{ timestamps: true }
);

mongoose.model("Question", QuestionSchema);
