const mongoose = require("mongoose");
const User = mongoose.model("User");

module.exports = {
	async getAll(req, res) {
		const registros = await User.find({});
		return res.json(registros);
	},
	async getByID(req, res) {
		const registro = await User.findById(req.params.id);
		return res.json(registro);
	},
	async incluir(req, res) {
		try {
			const registro = await User.create(req.body);
			return res.json(registro);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg });
		}
	},
	async editar(req, res) {
		const registro = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.json(registro);
	},
	async excluir(req, res) {
		await User.findByIdAndRemove(req.params.id);
		return res.send();
	}
};
