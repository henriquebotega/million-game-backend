const mongoose = require("mongoose");
const Game = mongoose.model("Game");

module.exports = {
	async getAll(req, res) {
		const registros = await Game.find({})
			.populate("_user")
			.populate("_level")
			.populate("_question")
			.populate("_fiftyQuestion")
			.populate("_skippedQuestions");
		return res.json(registros);
	},
	async getByID(req, res) {
		const registro = await Game.findById(req.params.id)
			.populate("_user")
			.populate("_level")
			.populate("_question")
			.populate("_fiftyQuestion")
			.populate("_skippedQuestions");
		return res.json(registro);
	},
	async incluir(req, res) {
		try {
			let registro = await Game.create(req.body);

			registro = await registro
				.populate("_user")
				.populate("_level")
				.populate("_question")
				.populate("_fiftyQuestion")
				.populate("_skippedQuestions")
				.execPopulate();

			return res.json(registro);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg });
		}
	},
	async editar(req, res) {
		const registro = await Game.findByIdAndUpdate(req.params.id, req.body, { new: true })
			.populate("_user")
			.populate("_level")
			.populate("_question")
			.populate("_fiftyQuestion")
			.populate("_skippedQuestions");
		return res.json(registro);
	},
	async excluir(req, res) {
		await Game.findByIdAndRemove(req.params.id);
		return res.send();
	},
	async skip(req, res) {
		const oldRegistro = await Game.findById(req.params.id);

		oldRegistro.skip = oldRegistro.skip - 1;
		oldRegistro._skippedQuestions.push(req.params.skip);

		const registro = await Game.findByIdAndUpdate(req.params.id, oldRegistro, { new: true })
			.populate("_user")
			.populate("_level")
			.populate("_question")
			.populate("_fiftyQuestion")
			.populate("_skippedQuestions");

		return res.json(registro);
	},
	async fifty(req, res) {
		const oldRegistro = await Game.findById(req.params.id);

		oldRegistro.fifty = true;
		oldRegistro._fiftyQuestion = req.params.fifty;

		const registro = await Game.findByIdAndUpdate(req.params.id, oldRegistro, { new: true })
			.populate("_user")
			.populate("_level")
			.populate("_question")
			.populate("_fiftyQuestion")
			.populate("_skippedQuestions");

		return res.json(registro);
	},
	async finishing(req, res) {
		const oldRegistro = await Game.findById(req.params.id);
		oldRegistro.reward = req.body.reward;

		await Game.findByIdAndUpdate(req.params.id, oldRegistro);
		return res.status(200).send();
	},
	async topten(req, res) {
		const registros = await Game.find({})
			.sort({ createdAt: "desc" })
			.populate("_user")
			.limit(10);
		return res.json(registros);
	}
};
