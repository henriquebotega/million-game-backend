const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const requireDir = require("require-dir");
const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://usuario:senha@banco-g6pxw.mongodb.net/test?retryWrites=true&w=majority", { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const server = require("http").Server(app);
const io = require("socket.io")(server);

app.use((req, res, next) => {
	req.io = io;
	return next();
});

requireDir("./src/models");
app.use("/api", require("./src/routes"));

server.listen(process.env.PORT || 3333);
