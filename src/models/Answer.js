const mongoose = require("mongoose");

const AnswerSchema = mongoose.Schema(
	{
		description: { type: String, require: true },
		isValid: { type: Boolean, default: false },
		isDisabled: { type: Boolean, default: false },
		_question: { ref: "Question", type: mongoose.Schema.Types.ObjectId }
	},
	{ timestamps: true }
);

mongoose.model("Answer", AnswerSchema);
