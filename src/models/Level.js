const mongoose = require("mongoose");

const LevelSchema = mongoose.Schema(
	{
		description: { type: String, require: true },
		rewardWin: { Type: Number },
		rewardGiveup: { Type: Number }
	},
	{ timestamps: true }
);

mongoose.model("Level", LevelSchema);
