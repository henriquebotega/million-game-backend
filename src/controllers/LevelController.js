const mongoose = require("mongoose");
const Level = mongoose.model("Level");

module.exports = {
	async getAll(req, res) {
		const registros = await Level.find({});
		return res.json(registros);
	},
	async getByID(req, res) {
		const registro = await Level.findById(req.params.id);
		return res.json(registro);
	},
	async incluir(req, res) {
		try {
			const registro = await Level.create(req.body);
			return res.json(registro);
		} catch (e) {
			return res.status(404).send({ error: e.errmsg });
		}
	},
	async editar(req, res) {
		const registro = await Level.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.json(registro);
	},
	async excluir(req, res) {
		await Level.findByIdAndRemove(req.params.id);
		return res.send();
	}
};
