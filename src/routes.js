const express = require("express");
const routes = express.Router();

const QuestionController = require("./controllers/QuestionController");
routes.get("/question", QuestionController.getAll);
routes.get("/question/:id", QuestionController.getByID);
routes.post("/question", QuestionController.incluir);
routes.put("/question/:id", QuestionController.editar);
routes.delete("/question/:id", QuestionController.excluir);

const AnswerController = require("./controllers/AnswerController");
routes.get("/answer", AnswerController.getAll);
routes.get("/answer/:id", AnswerController.getByID);
routes.post("/answer", AnswerController.incluir);
routes.put("/answer/:id", AnswerController.editar);
routes.delete("/answer/:id", AnswerController.excluir);

const UserController = require("./controllers/UserController");
routes.get("/user", UserController.getAll);
routes.get("/user/:id", UserController.getByID);
routes.post("/user", UserController.incluir);
routes.put("/user/:id", UserController.editar);
routes.delete("/user/:id", UserController.excluir);

const GameController = require("./controllers/GameController");
routes.get("/game", GameController.getAll);
routes.get("/game/:id", GameController.getByID);
routes.post("/game", GameController.incluir);
routes.put("/game/:id", GameController.editar);
routes.delete("/game/:id", GameController.excluir);

const LevelController = require("./controllers/LevelController");
routes.get("/level", LevelController.getAll);
routes.get("/level/:id", LevelController.getByID);
routes.post("/level", LevelController.incluir);
routes.put("/level/:id", LevelController.editar);
routes.delete("/level/:id", LevelController.excluir);

// Return all questions of a specific level
routes.get("/all/question/level/:level", QuestionController.getAllQuestionByLevel);
// Return one question by level (random)
routes.post("/question/level/:level", QuestionController.getByLevel);
// Return 4 answers of a question (1 = true, 3 = false and random)
routes.get("/answer/question/:id", AnswerController.getAnswerByQuestion);
// Skip a question
routes.put("/game/:id/skip/:skip", GameController.skip);
// Remove half of answers
routes.put("/game/:id/fifty/:fifty", GameController.fifty);
// Finishing a game
routes.put("/game/:id/finishing", GameController.finishing);
// Return top 10
routes.get("/topten", GameController.topten);

module.exports = routes;
